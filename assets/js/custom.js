$(function () {
    "use strict";

    // Initiate the wowjs animation library
    new WOW().init();

    // Preloader JS
    $(document).ready(function () {
        setTimeout(function () {
            $('#preloader').slideUp('slow');
        }, 2000);
    });

    // Nav link href scroll animation
    $('.nav-link-scroll').click(function () {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500);
        return false;
    });

    // Navbar Fixed 
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 10) {
            $('.navbar').addClass('active');
        } else {
            $('.navbar').removeClass('active');
        }
    });

    // Mobile Navbar collapse hide
    $('.mobile-navbar').on('click', function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Header Banner
    $('#home').backstretch([
        "assets/images/sliders/1.jpg"
        , "assets/images/sliders/2.jpg"
        , "assets/images/sliders/3.jpg"
    ], {
        fade: 750,
        duration: 3000
    });

    // Disable form submissions if there are invalid fields
    window.addEventListener('load', function () {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    //Alphaonly Validation
    $(".alphaonly").on("input", function () {
        var regexp = /[^-_ a-zA-Z,.]/g;
        if ($(this).val().match(regexp)) {
            $(this).val($(this).val().replace(regexp, ''));
        }
    });

    // Numberonly Validation 
    $(".numericonly").on("input", function () {
        var regexp = /[^0-9]/g;
        if ($(this).val().match(regexp)) {
            $(this).val($(this).val().replace(regexp, ''));
        }
    });

    // Client Carousel
    $('#clients-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoPlay: true,
        pagination: false,
        nav: true,
        navigation: true,
        navigationText: [
            "<i class='fa fa-angle-left fa-3x'></i>",
            "<i class='fa fa-angle-right fa-3x'></i>"
        ],
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 3],
        itemsMobile: [479, 1]
    });

    // Testimonials Carousel
    $('#testimonials-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoPlay: true,
        pagination: false,
        nav: true,
        navigation: true,
        navigationText: [
            "<i class='fa fa-angle-left fa-3x'></i>",
            "<i class='fa fa-angle-right fa-3x'></i>"
        ],
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1]
    });

    // Portfolio Gallery


    $(document).ready(function () {
        var $grid = $('.grid').imagesLoaded(function () {
            // init Isotope after all images have loaded
            $grid.isotope({
                // options...
                itemSelector: '.grid-item',
                percentPosition: true,
                masonry: {
                    columnWidth: '.grid-sizer'
                }
            });
        });
        // filter items on button click
        $('.portfolio-menu').on('click', 'button', function () {
            $('button').removeClass('active');
            $(this).addClass('active');
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({ filter: filterValue });
            return false;
        });
    });

    // Tooltip
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    // Back to top scroll
    $('#scrolltop').on("click", function () {
        $("html").animate({ scrollTop: 0 }, "slow");
    });

});







